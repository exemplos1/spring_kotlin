package br.com.nomeempresa.aula1.controller

import br.com.nomeempresa.aula1.service.Aula1Service
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("api/v1")
class Aula1Controller(private val aula1Service: Aula1Service) {

    @GetMapping("mod2/{numero}")
    fun mod2(@PathVariable numero: Int): Int {
        return aula1Service.mod(numero)
    }

    @GetMapping("mod3/{numero}")
    fun mod3(@PathVariable numero: Int): Int {
        return aula1Service.mod(numero, 3)
    }

    @GetMapping("mod3/{numero}/{mod}")
    fun modulo(@PathVariable numero: Int, @PathVariable mod: Int): Int {
        return aula1Service.mod(numero, mod)
    }

}