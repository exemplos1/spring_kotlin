package br.com.nomeempresa.aula1

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class Aula1Application

fun main(args: Array<String>) {
    runApplication<Aula1Application>(*args)
}
