package br.com.nomeempresa.aula1.model;

import java.util.Objects;

public class Aula {

    private Integer dia;
    private String mes;

    public Aula() {
    }

    public Aula(Integer dia) {
        this.dia = dia;
    }

    public Aula(String mes) {
        this.mes = mes;
    }

    public Aula(Integer dia, String mes) {
        this.dia = dia;
        this.mes = mes;
    }

    public Integer getDia() {
        return dia;
    }

    public void setDia(Integer dia) {
        this.dia = dia;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    @Override
    public String toString() {
        return "Aula{" +
                "dia=" + dia +
                ", mes='" + mes + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Aula aula = (Aula) o;
        return Objects.equals(dia, aula.dia) && Objects.equals(mes, aula.mes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dia, mes);
    }
}